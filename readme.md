# Code Samples

This repo contains some samples of code that I have recently written including both backend (Laravel) and frontend (VueJS).

# Example - Capabilities Database
A database of all our staff allowing them to record various items such as their technical capabilities, professional memberships and registrations.
This example shows some code behind the workings of the user profile page. This can be found in the `capabilities/vue/profile` directory.

## Frontend - User profile page

![alt text](/capabilities/profile.png "capabilites profile")

---

## Backend RESTful API

Since this application is an VueJS SPA, the backend is a simple RESTful API. This example shows part of the API that handles CRUD operations for the user's professional registration section of their profile page. This can be found in the `capabilities/laravel` directory.

![alt text](/capabilities/add-registration.png "add registration")