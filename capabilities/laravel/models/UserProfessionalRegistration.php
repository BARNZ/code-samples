<?php

namespace App\Models;

use App\Traits\AttachedToUser;
use App\Traits\HasSingleFileAttachment;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class UserProfessionalRegistration extends Model implements HasMedia
{
    use AttachedToUser, Searchable, HasSingleFileAttachment;

    protected $fillable = ['expiry_date'];

    public function registration()
    {
        return $this->belongsTo(Lookup::class);
    }

    /**
     * Load some relations for this model to include when listing in a table or similar.
     */
    public function scopeListing($q)
    {
        $q->select([
            'user_professional_registrations.*',
            'r.name as registration',
        ])
            ->leftJoin('professional_registrations as r', 'r.id', '=', 'user_professional_registrations.registration_id');
    }
}
