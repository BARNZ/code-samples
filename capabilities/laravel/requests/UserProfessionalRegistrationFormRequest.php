<?php

namespace App\Http\Requests\API\v1;

use Illuminate\Foundation\Http\FormRequest;

class UserProfessionalRegistrationFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_id'         => 'required|valid_id',
            'registration_id' => 'required|valid_id',
            'expiry_date'     => 'nullable|date',
            'attachment'      => 'nullable|valid_file',
        ];
    }

    public function attributes()
    {
        return [
            'registration_id' => 'registration',
        ];
    }
}
