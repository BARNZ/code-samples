<?php

///////////////////
// Authed Routes //
///////////////////

// All following api routes protected by laravel sanctum auth
Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('user-professional-registrations', 'UserProfessionalRegistrationsController');
});
