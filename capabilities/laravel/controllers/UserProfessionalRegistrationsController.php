<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\v1\UserProfessionalRegistrationFormRequest;
use App\Models\ProfessionalRegistration;
use App\Models\User;
use App\Models\UserProfessionalRegistration;
use Illuminate\Http\Request;

class UserProfessionalRegistrationsController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|valid_id',
        ]);

        $builder = UserProfessionalRegistration::listing()
            ->with('attachment')
            ->where('user_id', $request->user_id);

        return datatables()
            ->of($builder)
            ->escapeColumns([])
            ->toJson();
    }

    public function store(UserProfessionalRegistrationFormRequest $request)
    {
        return $this->handleForm($request, new UserProfessionalRegistration);
    }

    public function update(UserProfessionalRegistrationFormRequest $request, UserProfessionalRegistration $userProfessionalRegistration)
    {
        return $this->handleForm($request, $userProfessionalRegistration);
    }

    public function destroy(UserProfessionalRegistration $userProfessionalRegistration)
    {
        $this->authorize('update', $userProfessionalRegistration->user);
        $userProfessionalRegistration->delete();
    }

    private function handleForm(UserProfessionalRegistrationFormRequest $request, UserProfessionalRegistration $userProfessionalRegistration)
    {
        $user = User::findOrFail($request->user_id);
        $this->authorize('update', $user);

        $userProfessionalRegistration->fill($request->all());
        $userProfessionalRegistration->registration()->associate(ProfessionalRegistration::findOrFail($request->registration_id));
        $userProfessionalRegistration->user()->associate($user);
        $userProfessionalRegistration->save();

        updateFileAttachmentFromRequest($request, $userProfessionalRegistration, 'attachment');

        return $userProfessionalRegistration;
    }
}
