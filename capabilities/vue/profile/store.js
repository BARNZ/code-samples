import * as ProfileTabs from './tabs'
import { get } from 'utils/resource'
import AccordionItem from 'classes/accordionItem'
import { mapCollationToAccordionItem } from 'utils/capability'

/**
 * Shared profile state
 */
export default {
    namespaced: true,

    state: () => ({
        user: {},
        profileLastUpdated: null,
    }),

    mutations: {
        setUser: (state, user) => {
            state.user = user
            state.profileLastUpdated = user.profile_last_updated
        },

        setProfileLastUpdated: (state, timestamp) => {
            state.profileLastUpdated = timestamp
        }
    },

    actions: {
        async loadUser({ commit }, userId) {
            let user = await get('users', userId, { with_profile_info: true }).catch(() => {}) // ignore user not found error
            commit('setUser', user || {})
            return user
        },

        triggerProfileUpdate({ dispatch, commit, state }, timestamp) {
            commit('setProfileLastUpdated', timestamp)
            dispatch('loadUser', state.user.id) // reload user data to refresh things like counts etc.
        },
    },

    getters: {
        sections: (state, getters, rootState) => {
            if (!state.user.id) return []

            return [
                new AccordionItem({
                    label: 'Capabilities',
                    icon: rootState.icons.capability,
                    key: 'capabilities',
                    description: 'Technical and professional skills and competencies',
                    count: state.user.num_approved_capabilities,
                    component: ProfileTabs.CapabilitiesTab
                }),

                // Create an additional accordion item for each of the users collations
                ...state.user.collations.map(collation => mapCollationToAccordionItem(
                    collation,
                    ProfileTabs.CollationTab,
                    rootState.app.rules.rating_colour_map
                )),

                new AccordionItem({
                    label: 'Languages',
                    icon: rootState.icons.language,
                    key: 'languages',
                    description: 'Languages of the world fluency',
                    count: state.user.num_languages,
                    component: ProfileTabs.LanguagesTab
                }),
                new AccordionItem({
                    label: 'Countries of Experience',
                    icon: rootState.icons.country,
                    key: 'countries',
                    description: 'Countries of the world work experience',
                    count: state.user.num_countries,
                    component: ProfileTabs.CountriesOfExperienceTab
                }),
                new AccordionItem({
                    label: 'Professional Experience / History',
                    icon: rootState.icons.experience,
                    key: 'experience',
                    description: 'Work history on projects and with other employers',
                    count: state.user.num_professional_experiences + state.user.num_professional_history,
                    component: ProfileTabs.ProfessionalExperienceTab
                }),
                new AccordionItem({
                    label: 'Professional Registrations / Memberships',
                    icon: rootState.icons.registration,
                    key: 'registrations',
                    description: 'Professional registrations and memberships',
                    count: state.user.num_professional_memberships + state.user.num_professional_registrations,
                    component: ProfileTabs.ProfessionalRegistrationsTab
                }),
                new AccordionItem({
                    label: 'Professional Achievements (Awards / Presentations / Publications)',
                    icon: rootState.icons.achievement,
                    key: 'achievements',
                    description: 'Professional awards, presentations & publications',
                    count: state.user.num_professional_achievements,
                    component: ProfileTabs.ProfessionalAchievementsTab
                }),
                new AccordionItem({
                    label: 'Health & Safety / Certificates / Security Clearances',
                    icon: rootState.icons.health_safety,
                    key: 'certificates',
                    description: 'Health, Safety & Security related certificates and work clearances',
                    count: state.user.num_hs_certs + state.user.num_security_clearances + state.user.num_medicals,
                    component: ProfileTabs.HealthSafetyTab
                }),
                new AccordionItem({
                    label: 'PMAF',
                    icon: rootState.icons.pmaf,
                    key: 'pmaf',
                    description: 'Project Manager Accreditation Framework (PMAF) accreditation – NZ only',
                    count: state.user.num_pmafs,
                    component: ProfileTabs.PmafTab
                }),
                new AccordionItem({
                    label: 'Mentoring',
                    icon: rootState.icons.mentoring,
                    key: 'mentoring',
                    description: 'Manage your mentoring areas and agreements',
                    count: state.user.num_mentoring_areas + (state.user.can_edit_profile ? state.user.num_mentoring_agreements : 0),
                    component: ProfileTabs.MentoringTab,
                }),
                new AccordionItem({
                    label: 'Education History',
                    icon: rootState.icons.education,
                    key: 'education',
                    description: 'Education History',
                    count: state.user.num_education,
                    component: ProfileTabs.EducationTab
                }),
                new AccordionItem({
                    label: 'Documents / CVs',
                    icon: rootState.icons.documents,
                    key: 'documents',
                    description: 'Attached documents and CVs',
                    count: state.user.num_documents,
                    component: ProfileTabs.DocumentsTab
                }),
            ]
        },
        isMyProfile: (state, getters, rootState) => state.user.id == rootState.user.id,
        editRoute: (state) => ({
            name: 'edit_user',
            params: {
                id: state.user.id
            }
        }),
        managerProfileRoute(state) {
            return {
                to: 'profile',
                params: {
                    id: state.user.manager_id
                }
            }
        }
    }
}
