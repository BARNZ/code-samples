<!-- Represents a type of entity that can be managed from the user profile -->
<!-- Handles wiring up the functionality for displaying results and showing the form to allow creation and updating -->
<template>
    <div>
        <slot name="heading"></slot>

        <datatable ref="datatable" v-bind="mergedTableConfig" @loaded="$emit('loaded')">
            <slot name="datatable-controls"></slot>
        </datatable>

        <button v-if="!editOnly && !readOnly && user.can_update_profile" :disabled="addButtonDisabled" @click="editRecord()" class="btn btn-primary">
            <span class="fal fa-plus mr-1"></span>
            <span v-if="addButtonText">{{ addButtonText }}</span>
            <span v-else>Add {{ entity }}</span>
        </button>

        <slot name="actions"></slot>

        <b-modal
            v-if="form"
            v-model="showFormModal"
            v-bind="modalOpts"
            :title="modalTitle"
            dialog-class="modal-dialog-centered"
            @shown="onModalShown"
        >
            <form ref="form" @submit.stop.prevent="saveRecord">
                <info>Please fill in all required fields marked with a <span class="required"></span></info>

                <slot v-bind:form="form"></slot>
                <button class="d-none" type="submit"></button>
            </form>

            <template slot="modal-footer">
                <div class="flex-grow-1">
                    <action-button
                        v-if="form.id"
                        :action="deleteRecord"
                        :is-busy="isSaving"
                        class="mr-auto"
                        type="delete"
                    />
                </div>
                <button @click="showFormModal = false" :disabled="isSaving" class="btn btn-light">Cancel</button>
                <action-button :action="saveRecord" :is-busy="isSaving"></action-button>
            </template>
        </b-modal>
    </div>
</template>

<script>
    import DatatableColumnBuilder from 'components/datatables/datatableColumnBuilder'
    import Datatable from 'components/datatables/Datatable'
    import { attachToFormData } from 'utils/misc'
    import { genericToast } from 'utils/toasts'
    import { confirmDeleteModal } from 'utils/modals'
    import { save, destroy } from 'utils/resource'

    export default {
        props: {
            // Singluar name of entity being managed
            entity: {
                type: String,
                required: true
            },
            // Rest resource being managed
            resource: {
                type: String,
                required: true
            },
            // A preconfigured column builder to display the table with
            columnBuilder: {
                type: Object,
                required: true
            },
            // Optional function that can be used to prepare the form data
            // Default behaviour is to just copy all record properties into a new form object
            prepareForm: {
                type: Function,
                default: record => ({...record})
            },
            // Enable to prevent creation of new records
            editOnly: {
                type: Boolean,
                default: false
            },
            // Disables the ability to edit records (create and delete only)
            noEdit: {
                type: Boolean,
                default: false
            },
            // Force the delete button to always show
            showDelete: {
                type: Boolean,
                default: false
            },
            // Custom editing function (overrides default editing behaviour) Must return truthy to indicate edit behaviour was handled
            editFunction: {
                type: Function,
            },
            // Enable to prevent creation or editing of records
            readOnly: {
                type: Boolean,
                default: false
            },
            // Disable to prevent auto-focusing of the first modal input
            disableInputAutofocus: {
                type: Boolean,
                default: false
            },
            // Any additional filters to send with each request
            tableFilters: {
                type: Object,
                required: false
            },
            // Any additional table actions to provide
            additionalTableActions: {
                type: Array,
                required: false,
                default: () => ([])
            },
            // Custom "Add button" text to display
            addButtonText: {
                type: String,
            },
            // Disable the "Add button" but still show it
            addButtonDisabled: {
                type: Boolean,
                default: false
            },
            // Any additional datatable config overrides
            tableConfig: {
                type: Object,
                required: false,
                default: () => ({})
            },
            // Optional function to serialise the form data to send to the server on save
            get: {
                type: Function,
                default: form => form // by default just returns all form fields
            }
        },

        components: {
            Datatable
        },

        data() {
            return {
                selectedDate: null,
                form: null,
                isSaving: false,
                showFormModal: false,
                previouslyLoadedUserId: null,
                modalOpts: {
                    size: 'lg',
                    okVariant: 'success',
                    noCloseOnBackdrop: true, // prevent user accidentally wiping their form
                    hideHeaderClose: true, // not really needed
                    noEnforceFocus: true, // prevent modal blocking focus from other overlay elements such as sweetalerts
                }
            }
        },

        computed: {
            user() {
                return this.$store.state.profile.user
            },

            isEditing() {
                return this.form && this.form.id
            },

            modalTitle() {
                let type = this.isEditing ? 'Editing' : 'Add New'
                return `${type} ${this.entity}`
            },

            mergedTableConfig() {
                let tableActions = []

                // Include the standard row edit button?
                if (!this.noEdit) {
                    tableActions.push({
                        name: 'Edit',
                        icon: 'fa-edit',
                        variant: 'warning',
                        visible: this.user.can_update_profile,
                        onClick: this.editRecord
                    })
                }

                // Includea row delete button? Normally deletion is done within the edit modal
                if (this.noEdit || this.showDelete) {
                    tableActions.push({
                        name: 'Delete',
                        icon: 'fa-trash-alt',
                        variant: 'danger',
                        visible: this.user.can_update_profile,
                        onClick: this.deleteRecord
                    })
                }

                // Always create a set of default actions that will be the same across every table
                let actionColumns = new DatatableColumnBuilder()
                    .actionsColumn([
                        ...this.additionalTableActions,
                        ...tableActions
                    ])
                    .visible(!this.readOnly)

                return {
                    url: this.resource,
                    order: [[1, 'asc']],
                    sendData: () => ({
                        ...this.tableFilters,
                        user_id: this.user.id
                    }),

                    // tweak default table display so that it removes a bunch of clutter
                    simpleMode: true,
                    tableClasses: 'table-striped table-hover',

                    columns: this.columnBuilder.get().concat(actionColumns.get()),

                    ...this.tableConfig
                }
            }
        },

        methods: {
            refresh() {
                this.$refs.datatable.refresh()
            },

            // Try auto-focus first input when form modal is shown
            onModalShown(e) {
                let firstInput = e.target.querySelector('.form-control')
                if (firstInput && !this.disableInputAutofocus) {
                    firstInput.focus()
                }
            },

            async editRecord(record) {
                // Custom edit function override provided?
                if (this.editFunction && this.editFunction(record)) {
                    return // editFunction returning truthy means the edit was successfully handled
                }

                this.form = await this.prepareForm(record || {})
                this.showFormModal = true
            },

            saveRecord() {
                if (this.isSaving) return
                this.isSaving = true

                let formData = this.get(this.form)
                let payload = attachToFormData(formData)
                payload.append('user_id', this.user.id)

                return save(this.resource, this.form.id, payload)
                    .then(() => {
                        this.showFormModal = false
                        genericToast(
                            `The ${this.entity} was ${this.form.id ? 'updated on' : 'added to'} ${this.$store.getters['profile/isMyProfile'] ? 'your' : 'their'} profile`,
                            this.form.id ? 'Updated' : 'Added'
                        )
                        this.refresh()
                        this.$store.dispatch('profile/triggerProfileUpdate', moment())
                        this.$emit('saved', formData)
                    })
                    .catch(() => {}) // error already handled by global error handler, this prevents showing a console error from vue via native form submit handler
                    .finally(() => this.isSaving = false)
            },

            deleteRecord(record) {
                // depending on how this function was called, make sure we have a record to delete
                record = record || this.form

                return confirmDeleteModal(this.entity)
                    .then(() => destroy(this.resource, record.id))
                    .then(() => {
                        this.showFormModal = false
                        genericToast(
                            `The ${this.entity} was removed from ${this.$store.getters['profile/isMyProfile'] ? 'your' : 'their'} profile`,
                            'Removed'
                        )
                        this.refresh()
                        this.$store.dispatch('profile/triggerProfileUpdate', moment())
                    })
            }
        },

        watch: {
            user(user) {
                // Refresh only if the actual user ID changes
                // Since we can refresh the user data from the server resulting in a "different" user object
                // even through its actually the same user
                if (this.previouslyLoadedUserId != user.id) {
                    this.previouslyLoadedUserId = user.id
                    this.refresh()
                    this.showFormModal = false
                }
            },

            // Whenver any additional filters change, refresh the datatable
            tableFilters: {
                deep: true,
                handler() {
                    this.refresh()
                }
            }
        },

        mounted() {
            this.previouslyLoadedUserId = this.user ? this.user.id : null
        }
    }
</script>
